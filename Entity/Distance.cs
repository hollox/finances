//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hollox.Finances.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Distance
    {
        public int DistanceId { get; set; }
        public System.DateTime DistanceDate { get; set; }
        public Nullable<int> ContactId { get; set; }
        public int Distance1 { get; set; }
        public string Address { get; set; }
        public System.DateTime CreationDate { get; set; }
        public System.DateTime Modificationdate { get; set; }
    
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
        public virtual User User2 { get; set; }
    }
}

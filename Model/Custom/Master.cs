﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hollox.Finances.Model
{
    public class Master : Model
    {
        private Repository.Master _Repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnectionString"></param>
        public Master(string strConnectionString)
        {
            _Repository = new Repository.Master(strConnectionString);
            _RepositoryToDispose = _Repository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<Entity.AccountType> GetAccountTypes()
        {
            return _Repository.GetAccountTypes();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<Entity.Bank> GetBanks()
        {
            return _Repository.GetBanks();
        }
    }
}

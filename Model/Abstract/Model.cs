﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hollox.Finances.Model
{
    public abstract class Model : IDisposable
    {
        protected Hollox.Finances.Repository.Repository _RepositoryToDispose;

        public void Dispose()
        {
            _RepositoryToDispose.Dispose();
        }
    }
}

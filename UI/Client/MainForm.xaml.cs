﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hollox.Finances.Client
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainForm : Window
    {
        public IList<Entity.AccountType> accountTypeList { get; set; }
        public IList<Entity.Bank> bankList { get; set; }
        public IList<Entity.User> userList { get; set; }

        private string strConnectionString  = "metadata=res://*/Finances.csdl|res://*/Finances.ssdl|res://*/Finances.msl;"
                                            + "provider=System.Data.SqlClient;"
                                            + "provider connection string='data source=localhost;initial catalog=hlx_finances;integrated security=True;MultipleActiveResultSets=False;App=FinancesClient'";

        /// <summary>
        /// 
        /// </summary>
        public MainForm()
        {
            // create components
            InitializeComponent();

            // initialize lists
            using (Model.Master oModel = new Model.Master(strConnectionString))
            {
                accountTypeList = oModel.GetAccountTypes();
                bankList = oModel.GetBanks();
                //userList = oModel.GetUser();
            }

            // assign list to grid
            gridAccountType.ItemsSource = accountTypeList;
            gridBank.ItemsSource = bankList;
            gridUser.ItemsSource = userList;

            // disable cache
            ((DevExpress.Data.BaseGridController)gridAccountType.DataController).ValuesCacheMode = DevExpress.Data.CacheRowValuesMode.Disabled;
            ((DevExpress.Data.BaseGridController)gridBank.DataController).ValuesCacheMode = DevExpress.Data.CacheRowValuesMode.Disabled;
            ((DevExpress.Data.BaseGridController)gridUser.DataController).ValuesCacheMode = DevExpress.Data.CacheRowValuesMode.Disabled;

            // assign panel to menu for managing visibility
            chkAccountType.Tag = pnlAccountType;
            pnlAccountType.Tag = chkAccountType;

            chkBank.Tag = pnlBank;
            pnlBank.Tag = chkBank;

            chkUser.Tag = pnlUser;
            pnlUser.Tag = chkUser;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DockLayoutManager_DockItemClosed(object sender, DevExpress.Xpf.Docking.Base.DockItemClosedEventArgs e)
        {
            DevExpress.Xpf.Bars.BarCheckItem oCheckItem;
            DevExpress.Xpf.Docking.Base.DockItemClosedEventArgs ea = e as DevExpress.Xpf.Docking.Base.DockItemClosedEventArgs;

            foreach (DevExpress.Xpf.Docking.BaseLayoutItem oPanel in ea.AffectedItems)
            {
                oCheckItem = (DevExpress.Xpf.Bars.BarCheckItem)oPanel.Tag;
                oCheckItem.IsChecked = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarCheckItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            DevExpress.Xpf.Bars.BarCheckItem oCheckItem = (DevExpress.Xpf.Bars.BarCheckItem)e.Item;
            DevExpress.Xpf.Docking.BaseLayoutItem oPanel = (DevExpress.Xpf.Docking.BaseLayoutItem)oCheckItem.Tag;

            if (oCheckItem.IsChecked.Value)
            {
                oPanel.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                oPanel.Visibility = System.Windows.Visibility.Hidden;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hollox.Finances.Repository
{
    public abstract class Repository : IDisposable
    {
        // constants
        public const string FIELD_CREATION_USER_ID = "CreationUserId";
        public const string FIELD_CREATION_DATE = "CreationDate";

        // members
        protected readonly financesEntities _Dc;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnectionString"></param>
        public Repository(string strConnectionString)
        {
            _Dc = new financesEntities(strConnectionString);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _Dc.Dispose();
        }
    }
}

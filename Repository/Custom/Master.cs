﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hollox.Finances.Repository
{
    public class Master : Repository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnectionString"></param>
        public Master(string strConnectionString) : base(strConnectionString)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<Entity.AccountType> GetAccountTypes()
        {
            return (from a in _Dc.AccountType
                    select a).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<Entity.Bank> GetBanks()
        {
            return (from a in _Dc.Bank
                    select a).ToList();
        }
    }
}
